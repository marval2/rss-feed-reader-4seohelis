import { LocalStorageService } from 'angular-2-local-storage';
import { FeedEntry } from './../model/feed-entry';
import { FeedInfo } from './../model/feed-info';
import { Component, Input, ViewChild, OnInit, AfterViewInit, ViewEncapsulation } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';

@Component({
  selector: 'app-feed-card',
  templateUrl: './feed-card.component.html',
  styleUrls: ['./feed-card.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class FeedCardComponent implements OnInit, AfterViewInit {

  // single instance of RSS feed
  @Input() feed: any;
  selectedFeed = false;
  // modal variable
  @ViewChild('childModal') public childModal: ModalDirective;

  constructor(private localStorageService: LocalStorageService) { }

  // checks if this is the last link opened
  ngOnInit() {
    let temp;
    temp = this.localStorageService.get('lastSelectedLink');
    if (temp === this.feed.link) {
      this.selectedFeed = true;
    }
  }

  // Not sure if this is the actual task so I'll leave it commented out
  ngAfterViewInit() {
    /* if (this.selectedFeed) {
       this.showChildModal();
     }*/
  }

  public showChildModal(): void {
    this.selectedFeed = true;
    this.localStorageService.set('lastSelectedLink', this.feed.link);
    this.childModal.show();
  }

  public hideChildModal(): void {
    this.selectedFeed = false;
    this.childModal.hide();
  }
}
