import { FeedEntry } from './model/feed-entry';
import { FeedInfo } from './model/feed-info';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FeedService } from './feed.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  // holds all user feeds
  feeds = [];
  // information about user feeds. Managed in local storage
  feedInfos = new Array<FeedInfo>();
  // input form data
  urlForm: FormGroup;
  error = false;
  errorMessage: string;

  // for sorting
  abcSort = 0; // 0 = not sorted, 1 = sorted ascending, -1 = sorted descending
  dateSort = 0; // 0 = not sorted, 1 = sorted ascending, -1 = sorted descending

  constructor(
    private feedService: FeedService,
    private localStorageService: LocalStorageService
  ) {
    // Configuring form for RSS feed URL input. Can be extended for more validators etc
    this.urlForm = new FormGroup({
      'newFeedUrl': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {
    // this.clearLocalStorage();
    // Getting feeds information from localStorage
    let temp;
    temp = this.localStorageService.get('feedInfos');
    // If feed info in localstorage exists copy it to local variable feedInfos
    if (temp != null) {
      temp.forEach(info => {
        this.feedInfos.push(info);
      });
      console.log('Starting with these feeds: ');
      console.log(this.feedInfos);
      // Getting user feeds
      this.refreshFeed(); // no need to refresh if we know there are no feeds
    }
    console.log('No feeds to start with');
  }

  // for debugging. Clears local storage
  clearLocalStorage() {
    this.localStorageService.clearAll();
  }

  onSubmit() {
    this.getFeedInfo(this.urlForm.value.newFeedUrl);
  }

  onSortAbc() {
    if (this.abcSort === 0 || this.abcSort === -1) {
      this.feedService.sortByAbcAsc(this.feeds);
      this.abcSort = 1;
    } else {
      this.feedService.sortByAbcDesc(this.feeds);
      this.abcSort = -1;
    }
    this.dateSort = 0;
  }

  onSortDate() {
    if (this.dateSort === 0 || this.dateSort === -1) {
      this.feedService.sortByDateAsc(this.feeds);
      this.dateSort = 1;
    } else {
      this.feedService.sortByDateDesc(this.feeds);
      this.dateSort = -1;
    }
    this.abcSort = 0;

  }

  private resort() {
    if (this.dateSort === 1) {
      this.feedService.sortByDateAsc(this.feeds);
    } else if (this.dateSort === -1) {
      this.feedService.sortByDateDesc(this.feeds);
    } else if (this.abcSort === 1) {
      this.feedService.sortByAbcAsc(this.feeds);
    } else if (this.abcSort === -1) {
      this.feedService.sortByAbcDesc(this.feeds);
    }
  }

  determineAbcClasses() {
    let classes = '';
    classes += ' glyphicon';
    if (this.abcSort === 1) {
      classes += ' glyphicon-arrow-up active';
    } else if (this.abcSort === -1) {
      classes += ' glyphicon-arrow-down active';
    }
    return classes;
  }

  determineDateClasses() {
    let classes = '';
    classes += ' glyphicon';
    if (this.dateSort === 1) {
      classes += ' glyphicon-arrow-up active';
    } else if (this.dateSort === -1) {
      classes += ' glyphicon-arrow-down active';
    }
    return classes;
  }

  // add feed info if valid
  private addFeedUrl(feedInfo) {
    if (!this.feedInfos.some(url => feedInfo.link === url.link)) {
      this.feedInfos.push(feedInfo);
      this.localStorageService.set('feedInfos', this.feedInfos);
      this.refreshFeed();
    } else {
      this.error = true;
      this.errorMessage = 'You have already subscribed to this feed'; // data.message;
      // wait 5 Seconds and hide error message
      setTimeout(() => {
        this.error = false;
        this.errorMessage = '';
      }, 5000);
    }
  }

  // after user entered feed url, check if we can add it to his url list
  private getFeedInfo(url: string) {
    this.urlForm.reset();
    let data;
    if (url !== '' && url != null) {
      this.feedService.getFeedContent(url)
        .subscribe(
        feed =>
          data = feed,
        error => console.log(error),
        () => {
          if (data.status === 'error') {
            // show error message
            this.error = true;
            this.errorMessage = '<strong>Wrong URL!</strong> Please enter correct RSS feed URL'; // data.message;
            // wait 5 Seconds and hide error message
            setTimeout(() => {
              this.error = false;
              this.errorMessage = '';
            }, 5000);
          } else if (data.status === 'ok') { // only if feed status is ok we add feed url
            this.addFeedUrl(data.feed);
          }
        }
        );
    }
  }

  // Refresh feed list
  private refreshFeed() {
    this.feeds = [];
    let tempFeedInfos = [];
    // Leave this check just in case
    if (this.feedInfos.length > 0) {
      this.feedInfos.forEach(info => {
        this.feedService.getFeedContent(info.url)
          .subscribe(
          feed => {
            tempFeedInfos.push(feed.feed);
            feed.items.forEach(instance => {
              this.feeds.push(instance);
            });
          },
          error => console.log(error));
      });
    } else {
      this.resort();
      console.log('User has no feeds');
    }
  }
}
