# Rssfeeder

## Running project
1. Open terminal window in rssfeeder directory
2. Run `npm install`
3. Run `ng serve`
4. Navigate to `http://localhost:4200/`

##Example RSS feeds:
* http://www.lrytas.lt/rss/
* http://feeds.feedburner.com/technologijos-visos-publikacijos?format=xml