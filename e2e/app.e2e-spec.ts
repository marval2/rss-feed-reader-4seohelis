import { RssfeederPage } from './app.po';

describe('rssfeeder App', function() {
  let page: RssfeederPage;

  beforeEach(() => {
    page = new RssfeederPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
